﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastDemo1 : MonoBehaviour
{
    public Material TargetMaterial;

    private Renderer LastGameObject;
    private Material LastMaterial;

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            if (TargetMaterial != null)
            {
                var currentHit = hit.collider.gameObject.GetComponent<Renderer>();
                if (LastGameObject != currentHit)
                {
                    if (LastGameObject != null && LastMaterial != null)
                    {
                        LastGameObject.material = LastMaterial;
                    }
                    LastGameObject = currentHit;
                    if (LastGameObject != null)
                    {
                        LastMaterial = LastGameObject.material;
                        LastGameObject.material = TargetMaterial;
                    }
                }
            }

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.red);
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.white);

            if(LastGameObject != null && LastMaterial != null)
            {
                LastGameObject.material = LastMaterial;
                LastGameObject = null;
                LastMaterial = null;
            }
        }
    }
}
