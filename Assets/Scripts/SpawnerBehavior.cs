﻿using UnityEngine;

public class SpawnerBehavior : MonoBehaviour
{
    public GameObject Prefab;
    private int Counter = 0;

    void Start()
    {
    }

    void Update()
    {
        if(Counter++ > 20)
        {
            Counter = 0;
            SpawnAnObject();
        }
        
    }

    void SpawnAnObject()
    {
        if(Prefab != null)
        {
            var newObject = GameObject.Instantiate(Prefab);

            var scale = Random.Range(0.1f, 0.8f);
            newObject.transform.localScale = new Vector3(scale, scale, scale);
            //newObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

            newObject.transform.SetParent(this.transform, false);

            var body = newObject.GetComponent<Rigidbody>();
            if(body != null)
            {
                body.AddForce(Random.insideUnitSphere * 1.5f, ForceMode.Impulse); // adding a force on the center makes it move
                body.AddForceAtPosition(Random.insideUnitSphere * 30f, Random.onUnitSphere); // adding a force away from the center makes it rotate
            }
        }
    }
}
