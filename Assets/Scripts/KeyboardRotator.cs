﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardRotator : MonoBehaviour
{
    public float RotationSpeed = 90f;

    void Update()
    {
        var axis = Input.GetAxis("Horizontal");
        this.transform.Rotate(0, axis * RotationSpeed * Time.deltaTime, 0);
    }
}
