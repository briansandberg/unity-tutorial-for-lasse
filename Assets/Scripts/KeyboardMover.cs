﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardMover : MonoBehaviour
{
    // Start is called before the first frame update
    public float MovementSpeed = 0.1f;

    void Update()
    {
        var axis = Input.GetAxis("Vertical");
        this.transform.Translate(0,0, axis * MovementSpeed);
    }
}
